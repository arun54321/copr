%global ananicycppver 1.0.0-rc6
%global ananicycpp ananicy-cpp-v%{ananicycppver}
%global ananicyver 2.2.1
%global jsonver 3.10.5
%global fmtlibver 8.0.1
%global spdlogver 1.9.2

Name:    ananicy-cpp
Version: 1.0.0.rc6
Release: 1%{?dist}
Summary: The runtime of ananicy-cpp with the rules from the original ananicy.

Group:   System Environment/Daemons
License: GPLv3
URL:     https://gitlab.com/ananicy-cpp/ananicy-cpp/
Source0: https://gitlab.com/ananicy-cpp/ananicy-cpp/-/archive/v%{ananicycppver}/%{ananicycpp}.tar.gz
Source1: https://github.com/Nefelim4ag/Ananicy/archive/refs/tags/%{ananicyver}.tar.gz
Source2: https://github.com/nlohmann/json/archive/refs/tags/v%{jsonver}.tar.gz
Source3: https://gitlab.com/ananicy-cpp/stl-polyfills/std-format/-/archive/main/std-format-main.tar.gz



BuildRequires: cmake g++ git
BuildRequires: systemd-devel fmt-devel spdlog-devel libubsan
BuildRequires: systemd-rpm-macros
Requires:      systemd fmt spdlog libubsan

%description
ananicy-cpp is a rewrite of ananicy in C++ which targets lower CPU/Memory usage.
This package utilizes the executable of that combined with the rules from the
original ananicy project.


%package devel
Summary: ananicy-cpp libraries and header files


%description devel
ananicy-cpp libraries and header files


%prep

%setup -q -n %{ananicycpp}
%setup -q -T -D -a 1 -n %{ananicycpp}
%setup -q -T -D -a 2 -n %{ananicycpp}
%setup -q -T -D -a 3 -n %{ananicycpp}/external/

pushd %{_builddir}/%{ananicycpp}/external/
rmdir std-format
mv std-format-main std-format
ln -s ../redhat-linux-build .
popd

%build
%define work_dir %{_builddir}/%{ananicycpp}
%define json_dir %{work_dir}/json-%{jsonver}
cd %{json_dir}
cmake -S %{json_dir} -B %{json_dir}
cd %{work_dir}
%cmake -DENABLE_SYSTEMD=yes \
-DUSE_EXTERNAL_JSON=ON -Dnlohmann_json_DIR=%{json_dir} \
-DUSE_EXTERNAL_FMTLIB=ON -DUSE_EXTERNAL_SPDLOG=ON

%cmake_build

%install
%cmake_install
mkdir -p %{buildroot}%{_sysconfdir}
cp -a %{work_dir}/Ananicy-%{ananicyver}/ananicy.d %{buildroot}%{_sysconfdir}
mkdir -p %{buildroot}/usr/lib64
mv %{buildroot}/usr/lib/cmake %{buildroot}/usr/lib64/


%files
%{_bindir}/ananicy-cpp
%{_unitdir}/ananicy-cpp.service
%{_sysconfdir}/*

%files devel
%{_includedir}/polyfills/*
%{_libdir}/cmake/StlPolyfillFormat/*

%post
%systemd_post ananicy-cpp.service

%preun
%systemd_preun ananicy-cpp.service

%postun
%systemd_postun_with_restart ananicy-cpp.service

%changelog
* Sat Feb 26 2022 Arun G 1.0.0-rc6+2.1.1-1
- Update to rc6

* Wed Nov 10 2021 Harold Dost <github@hdost.com> 1.0.0-rc4+2.1.1-1
- Initial packaging.

